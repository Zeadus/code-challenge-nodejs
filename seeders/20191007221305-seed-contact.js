const faker = require('faker');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query('SELECT id from "City" ORDER BY id ASC').then((data) => {
      const contacts = [
        {
          identification: '2222222',
          name: 'Daniel Sanchez',
          email: 'danielsanchez@gmail.com',
          birthdate: '1996-06-24',
          company: 'Company',
          image: 'https://picsum.photos/200/300',
          address: 'CABA, Argentina',
          personalphone: '+584121607432',
          workphone: '1127595259',
          CityId: data[0][0].id
        }
      ];
      for (let i = 0; i < 20; i += 1) {
        const Contact = {
          identification: faker.random.number({ min: 7000000, max: 95000000 }).toString(),
          name: faker.name.findName(),
          email: faker.internet.email(),
          birthdate: faker.date.past(),
          company: faker.company.companyName(),
          image: 'https://picsum.photos/200/300',
          address: `${faker.address.streetAddress()}  ${faker.address.secondaryAddress()}`,
          personalphone: faker.phone.phoneNumber(),
          workphone: faker.phone.phoneNumber(),
          CityId: data[0][faker.random.number(5)].id
        };
        contacts.push(Contact);
      }
      return queryInterface.bulkInsert('Contact', contacts, {});
    });
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
  },

  down: (queryInterface) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('Contact', null, {});
  }
};
