module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query('SELECT id from "State" ORDER BY id ASC').then((data) => {
      return queryInterface.bulkInsert('City', [
        {
          name: 'Maracaibo',
          StateId: data[0][0].id
        },
        {
          name: 'Cabimas',
          StateId: data[0][0].id
        },
        {
          name: 'San Franisco',
          StateId: data[0][0].id
        },
        {
          name: 'Caracas',
          StateId: data[0][1].id
        },
        {
          name: 'San Cristobal',
          StateId: data[0][2].id
        },
        {
          name: 'El cobre',
          StateId: data[0][2].id
        },
      ], {});
    });
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('City', null, {});

    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
