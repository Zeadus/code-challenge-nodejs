module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query('SELECT id from "Country" ORDER BY id ASC').then((data) => {
      return queryInterface.bulkInsert('State', [
        {
          name: 'Zulia',
          CountryId: data[0][0].id,
        },
        {
          name: 'Distrito Capital',
          CountryId: data[0][0].id,
        },
        {
          name: 'Tachira',
          CountryId: data[0][0].id,
        }
      ], {});
    });
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('State', null, {});
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
