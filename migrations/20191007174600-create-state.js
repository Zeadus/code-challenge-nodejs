module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('State', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.STRING
      },
      CountryId: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
          model: 'Country',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      },
      createdAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('NOW()')
      },
      updatedAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('NOW()')
      }
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('State');
  }
};
