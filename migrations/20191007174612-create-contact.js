module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Contact', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      identification: {
        type: Sequelize.STRING(100),
        unique: true,
        allowNull: false
      },
      name: {
        type: Sequelize.STRING(100),
        unique: false,
        allowNull: false
      },
      email: {
        type: Sequelize.STRING(100),
        unique: false,
        allowNull: false
      },
      birthdate: {
        type: Sequelize.DATEONLY,
        unique: false,
        allowNull: false
      },
      company: {
        type: Sequelize.STRING(100),
        unique: false,
        allowNull: false
      },
      image: {
        type: Sequelize.STRING,
        unique: false,
        allowNull: true
      },
      address: {
        type: Sequelize.STRING(200),
        unique: false,
        allowNull: false
      },
      personalphone: {
        type: Sequelize.STRING(100),
        unique: false,
        allowNull: false
      },
      workphone: {
        type: Sequelize.STRING(100),
        unique: false,
        allowNull: true
      },
      CityId: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
          model: 'City',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      },
      createdAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('NOW()')
      },
      updatedAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('NOW()')
      }
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('Contact');
  }
};
