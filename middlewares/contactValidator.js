import { body, oneOf, query, param } from 'express-validator/check';

export default {
  createContact: [
    body('identification', 'a valid ID is required').exists().isAlphanumeric(),
    body('name', 'a valid name is required').exists(),
    body('birthdate', 'Birth date is required').exists().isISO8601(),
    body('company', 'a valid company is required').exists().isAlpha(),
    body('image', 'a valid Image is required').exists().isURL(),
    body('address', 'a valid Address is required').exists(),
    body('personalphone', 'a valid personal phone is required').exists().isAlphanumeric(),
    body('workphone', 'a valid work phone is required').exists().isAlphanumeric(),
    body('email', 'a valid email is required').exists().isEmail()
  ],

  updateContact: [
    param('id', 'a valid ID is required').exists().isNumeric(),
    body('identification', 'a valid identification number is required').exists().isNumeric(),
    body('name', 'a valid name is required').exists(),
    body('birthdate', 'Birth date is required').exists().isISO8601(),
    body('company', 'a valid company is required').exists().isAlpha(),
    body('image', 'a valid Image is required').exists().isURL(),
    body('address', 'a valid Address is required').exists(),
    body('personalphone', 'a valid personal phone is required').exists().isAlphanumeric(),
    body('workphone', 'a valid work phone is required').exists().isAlphanumeric(),
    body('email', 'a valid email is required').exists().isEmail()
  ],

  getContactByEmailOrPhone:
    oneOf([
      query('personalphone', 'personal phone is required').optional().isMobilePhone(),
      query('workphone', 'work phone is required').optional().isMobilePhone(),
      query('email', 'Invalid email').optional().isEmail()
    ]),

  getContactByAddress:
    oneOf([
      query('city', 'personal phone is required').optional().isNumeric(),
      query('state', 'work phone is required').optional().isNumeric(),
    ]),

  getContactById: [
    param('id', 'a valid ID is required').exists().isNumeric()
  ],

  deleteContact: [
    param('id', 'a valid ID is required').exists().isNumeric()
  ]

};
