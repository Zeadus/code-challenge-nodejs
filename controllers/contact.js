import { validationResult } from 'express-validator';
import { Sequelize } from 'sequelize';
import responseHandler from '../helpers/responseHandler';
import model from '../models';

const { Op } = Sequelize;

export default {
  async getContactByEmailOrPhone(req, res) {
    let response;
    const search = {
      [Op.and]: [
        { email: { [Op.like]: `%${req.query.email ? req.query.email : ''}%` } },
        { workphone: { [Op.like]: `%${req.query.workphone ? req.query.workphone : ''}%` } },
        { personalphone: { [Op.like]: `%${req.query.personalphone ? req.query.personalphone : ''}%` } }
      ]
    };
    try {
      const contacts = await model.Contact.findAll({ where: search });
      if (contacts.length > 0) {
        response = responseHandler.successResponse(200, contacts, 'Contacts retrieved successfully');
      } else {
        response = responseHandler.successResponse(200, null, 'No contacts Found');
      }
      return responseHandler.sendResponse(response, res);
    } catch (error) {
      console.log(error);
      return responseHandler.systemError(res);
    }
  },

  async getContactsByAddress(req, res) {
    let response;
    try {
      const contacts = await model.Contact.findAll({
        include: [{
          model: model.City,
          required: true,
          where: { id: req.query.city },
          include: [
            {
              model: model.State,
              required: true,
              where: { id: req.query.state },
              include: [{
                model: model.Country,
                required: true,
                attributes: ['name']
              }],
              attributes: ['name']
            }
          ],
          attributes: ['name']
        }]
      });
      if (contacts.length > 0) {
        response = responseHandler.successResponse(200, contacts, 'Contacts retrieved successfully');
      } else {
        response = responseHandler.successResponse(200, null, 'No contacts Found');
      }
      return responseHandler.sendResponse(response, res);
    } catch (error) {
      console.log(error);
      return responseHandler.systemError(res);
    }
  },

  async getContactById(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return responseHandler.validatorResponse(res, errors);
    }
    let response;
    try {
      const contacts = await model.Contact.findOne({
        where: {
          id: parseInt(req.params.id, 10)
        },
        include: [{
          model: model.City,
          required: true,
          include: [
            {
              model: model.State,
              required: true,
              include: [{
                model: model.Country,
                required: true,
                attributes: ['name']
              }],
              attributes: ['name']
            }
          ],
          attributes: ['name']
        }]
      });

      if (contacts) {
        response = responseHandler.successResponse(200, contacts, 'Contact retrieved successfully');
      } else {
        response = responseHandler.successResponse(404, null, 'No contact Found');
      }
      return responseHandler.sendResponse(response, res);
    } catch (error) {
      console.log(error);
      return responseHandler.systemError(res);
    }
  },

  async createContact(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return responseHandler.validatorResponse(res, errors);
    }
    let response;
    try {
      const contact = await model.Contact.findOne({
        where: { identification: req.body.identification }
      });
      if (contact) {
        response = responseHandler.errorResponse(400, 'Contact with that id already exists');
      }

      const city = await model.City.findOne({
        where: { id: req.body.CityId }
      });
      if (!city) {
        response = responseHandler.errorResponse(404, 'City does not exists');
      } else {
        await model.Contact.create(req.body);
        response = responseHandler.successResponse(201, 'Contact created successfully');
      }
      return responseHandler.sendResponse(response, res);
    } catch (error) {
      console.log(error);
      return responseHandler.systemError(res);
    }
  },

  async updateContact(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return responseHandler.validatorResponse(res, errors);
    }
    let response;
    try {
      let contact = await model.Contact.findOne({
        where: { id: parseInt(req.params.id, 10) }
      });
      if (!contact) {
        response = responseHandler.errorResponse(404, 'Contact with that id doesnt exists');
      }

      const city = await model.City.findOne({
        where: { id: req.body.CityId }
      });
      if (!city) {
        response = responseHandler.errorResponse(404, 'City does not exists');
      } else {
        if (contact.identification !== req.body.identification) {
          contact = await model.Contact.findOne({
            where: { identification: req.body.identification }
          });
        } else {
          contact = null;
        }

        if (contact) {
          response = responseHandler.errorResponse(409, 'Contact with that identification already exists');
        } else {
          await model.Contact.update(req.body, { where: { id: req.params.id } });
          response = responseHandler.successResponse(200, 'Contact updated successfully');
        }
      }
      return responseHandler.sendResponse(response, res);
    } catch (error) {
      console.log(error);
      return responseHandler.systemError(res);
    }
  },

  async deleteContact(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return responseHandler.validatorResponse(res, errors);
    }
    let response;
    try {
      const contact = await model.Contact.findOne({
        where: { id: parseInt(req.params.id, 10) }
      });
      if (!contact) {
        response = responseHandler.errorResponse(404, 'Contact with that id doesnt exists');
      } else {
        await model.Contact.destroy({ where: { id: parseInt(req.params.id, 10) } });
        response = responseHandler.successResponse(200, 'Contact deleted successfully');
      }
      return responseHandler.sendResponse(response, res);
    } catch (error) {
      console.log(error);
      return responseHandler.systemError(res);
    }
  }

};
