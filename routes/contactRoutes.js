import { Router } from 'express';
import ContactController from '../controllers/contact';
import ContactValidator from '../middlewares/contactValidator';

const router = Router();

router.get('/', ContactValidator.getContactByEmailOrPhone, ContactController.getContactByEmailOrPhone);
router.get('/byAddress', ContactValidator.getContactByAddress, ContactController.getContactsByAddress);
router.get('/:id', ContactValidator.getContactById, ContactController.getContactById);
router.post('/', ContactValidator.createContact, ContactController.createContact);
router.put('/:id', ContactValidator.updateContact, ContactController.updateContact);
router.delete('/:id', ContactValidator.deleteContact, ContactController.deleteContact);
export default router;
