import { Router } from 'express';
import contactRoutes from './contactRoutes';

const router = Router();

router.use('/contacts', contactRoutes);

export default router;
