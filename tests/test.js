import mocha from 'mocha';
import chai from 'chai';
import chaiHttp from 'chai-http';
import 'chai/register-should';
import app from '../app';

chai.use(chaiHttp);
const { expect } = chai;

mocha.describe('Contact EP testing', () => {
  it('should get all contacts', (done) => {
    chai.request(app)
      .get('/contacts/')
      .set('Accept', 'Application/Json')
      .end((err, res) => {
        expect(res.status).to.equal(200);
        res.body.data.should.be.a('array');
        res.body.data[0].should.have.property('id');
        res.body.data[0].should.have.property('identification');
        res.body.data[0].should.have.property('name');
        res.body.data[0].should.have.property('email');
        done();
      });
  });

  it('should filter contacts with @gmail domain', (done) => {
    chai.request(app)
      .get('/contacts/?email=@gmail')
      .set('Accept', 'Application/Json')
      .end((err, res) => {
        expect(res.status).to.equal(200);
        expect(res.body.data[0].email).to.include('@gmail');
        expect(res.body.data).to.be.a('array');
        res.body.data[0].should.have.property('id');
        res.body.data[0].should.have.property('identification');
        res.body.data[0].should.have.property('name');
        res.body.data[0].should.have.property('email');
        done();
      });
  });

  it('should get an specific contact', (done) => {
    chai.request(app)
      .get('/contacts/1')
      .set('Accept', 'Application/Json')
      .end((err, res) => {
        expect(res.status).to.equal(200);
        expect(res.body.data).to.be.a('object');
        expect(res.body.data.id).to.equal(1);
        res.body.data.should.have.property('id');
        res.body.data.should.have.property('identification');
        res.body.data.should.have.property('name');
        res.body.data.should.have.property('email');
        done();
      });
  });

  it('should not find an specific contact', (done) => {
    chai.request(app)
      .get('/contacts/10000')
      .set('Accept', 'Application/Json')
      .end((err, res) => {
        expect(res.status).to.equal(404);
        done();
      });
  });

  it('should update an specific contact', (done) => {
    const contact = {
      identification: '2222222222',
      name: 'Armando Sanchez',
      email: 'danielsanchez.1696@gmail.com',
      birthdate: '1996-06-24',
      company: 'Company',
      image: 'https://picsum.photos/200/300',
      address: 'CABA, Retiro, Argentina',
      personalphone: '584121607418',
      workphone: '1170595259',
      CityId: 1
    };
    chai.request(app)
      .put('/contacts/1')
      .set('Accept', 'Application/Json')
      .send(contact)
      .end((err, res) => {
        expect(res.status).to.equal(200);
        done();
      });
  });

  it('should create a new contact', (done) => {
    const contact = {
      identification: '26263264',
      name: 'Rigo Sanchez',
      email: 'rigosanchez@gmail.com',
      birthdate: '1994-11-15',
      company: 'Company',
      image: 'https://picsum.photos/200/300',
      address: 'CABA, Argentina',
      personalphone: '584121607418',
      workphone: '1170595259',
      CityId: 1
    };
    chai.request(app)
      .post('/contacts/')
      .set('Accept', 'Application/Json')
      .send(contact)
      .end((err, res) => {
        expect(res.status).to.equal(201);
        done();
      });
  });

  it('should delete a contact', (done) => {
    chai.request(app)
      .delete('/contacts/21')
      .set('Accept', 'Application/Json')
      .end((err, res) => {
        expect(res.status).to.equal(200);
        chai.request(app)
          .get('/contacts/21')
          .set('Accept', 'Application/Json')
          .end((e, r) => {
            expect(r.status).to.equal(404);
            done();
          });
      });
  });

  it('get at least 1 contact filtering by state and city', (done) => {
    chai.request(app)
      .get('/contacts/byAddress?city=1&state=1')
      .set('Accept', 'Application/Json')
      .end((err, res) => {
        expect(res.status).to.equal(200);
        expect(res.body.data[0].CityId).to.equal(1);
        done();
      });
  });
});
