require('dotenv').config();

module.exports = {
  development: {
    env: 'development',
    username: 'postgres',
    password: '',
    database: 'Solstice_userApp',
    host: '127.0.0.1',
    port: '5432',
    dialect: 'postgres',
    operatorsAliases: false
  },
  test: {
    env: 'test',
    username: 'postgres',
    password: '',
    database: 'Solstice_userApp',
    host: '127.0.0.1',
    port: '5432',
    dialect: 'postgres',
    operatorsAliases: false
  },
  production: {
    env: 'production',
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_USER,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: 'postgres',
    operatorsAliases: false
  }
};
