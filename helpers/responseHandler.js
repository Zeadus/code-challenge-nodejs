export default {
  successResponse(status, data, message) {
    return {
      status,
      data,
      message
    };
  },

  errorResponse(status, error) {
    return {
      status,
      error
    };
  },

  validatorResponse(res, errors) {
    return res.status(422).json({ errors: errors.array() });
  },

  systemError(res) {
    const response = {
      status: 500,
      data: null,
      message: 'Unexpected error'
    };
    return res.status(response.status).json(response);
  },

  sendResponse(json, res) {
    return res.status(json.status).json(json);
  }
};
