module.exports = (sequelize, DataTypes) => {
  const Contact = sequelize.define('Contact', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    identification: {
      type: DataTypes.STRING(100),
      unique: true,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING(100),
      unique: false,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(100),
      unique: false,
      allowNull: false
    },
    birthdate: {
      type: DataTypes.DATEONLY,
      unique: false,
      allowNull: false
    },
    company: {
      type: DataTypes.STRING(100),
      unique: false,
      allowNull: false
    },
    image: {
      type: DataTypes.STRING,
      unique: false,
      allowNull: true
    },
    address: {
      type: DataTypes.STRING(200),
      unique: false,
      allowNull: false
    },
    personalphone: {
      type: DataTypes.STRING(100),
      unique: false,
      allowNull: false
    },
    workphone: {
      type: DataTypes.STRING(100),
      unique: false,
      allowNull: true
    },
  }, {
    freezeTableName: true,
  });
  Contact.associate = (models) => {
    // associations can be defined here
    Contact.belongsTo(models.City);
  };
  return Contact;
};
