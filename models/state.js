module.exports = (sequelize, DataTypes) => {
  const State = sequelize.define('State', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: DataTypes.STRING(100)
  }, {
    freezeTableName: true,
  });
  State.associate = (models) => {
    // associations can be defined here
    State.belongsTo(models.Country);
    State.hasMany(models.City);
  };
  return State;
};
