module.exports = (sequelize, DataTypes) => {
  const City = sequelize.define('City', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: DataTypes.STRING
  }, {
    freezeTableName: true,
  });
  City.associate = (models) => {
    City.belongsTo(models.State);
    // associations can be defined here
  };
  return City;
};
