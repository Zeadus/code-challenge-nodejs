module.exports = (sequelize, DataTypes) => {
  const Country = sequelize.define('Country', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: DataTypes.STRING
  }, {
    freezeTableName: true,
  });
  Country.associate = (models) => {
    // associations can be defined here
    Country.hasMany(models.State);
  };
  return Country;
};
