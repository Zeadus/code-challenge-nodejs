This API was developed using NodeJs with ExpressJS, express-validator to validate input data, Sequelize with a Postgres DB for data persistence, Babel for ES6 and Mocha/Chai for unit testing.

To setup the API:
-Install sequelize globally (npm i -g Sequelize-cli)
-Run Npm install
-Create a DB in PostgreSQL and setup the connection in the config/config.js file
-Run the migrations ($ sequelize db:migrate:all)
-Run the seeders ($ sequelize db:seed:all)
-Start it (npm run dev)

To test the API run "npm run test"

To generate the compiled folder run "npm run build"

There is a postman collection on the root of the folder called "Solstice Challenge.postman_collection.json" 

This was developed in Windows 10 so it is possible that it wont work well in Linux



Note: I normally don't use Sequelize to handle migrations or create queries, i used Sequelize here because it was easier to develop the API, I normally use raw and sanitized SQL queries and Plpsql functions to insert/modify data


Folder structure:

-app.js: Main app file

-/routes: Routes are defined here, main routes like contacts (countries could be one as well) are defined in the index.js with a .js file for each main route

-/middlewares: All middlewares should go here. In this case i only developed the contactValidator. Auth middlewares and such should go here.

-/helpers: All helper classes/functinons should go here. In this case only the responseHandle was needed, which takes care of creating the Json and sending the response back to the client.

-/controllers: Controllers go here, 1 was enough but in case of a larger structure it could be divided in several controllers for a main route, also a service/middleware between the controller and the model could be developed in case sequelize calls become too long

-/models | /migrations | /seeders: These are sequelize folders. Migrations take care of creating tables/associations/updates. Seeders generate mock data for testing purposes

